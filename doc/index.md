---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "淳港同学"
  text: "Stay foolish, Stay hungry"
  tagline: /斜杠青年/人间清醒/核动力驴/
  image:
    # 首页右边的图片
    src: /assets/logo.jpg
    # 图片的描述
    alt: avatar
  actions:
    - theme: brand
      text: 进入主页
      link: /markdown-examples
    - theme: alt
      text: 个人成长
      link: /api-examples

features:
  - icon: 👱
    title: Web前端
    details: 小厂程序猿，国内某互联网厂搬砖。
  - icon: 🐴
    title: 核动力驴
    details: 先天打工圣体，专为拉磨而生。
  - icon: 🧩
    title: 斜杆青年
    details: 是个平平无奇但是又很热爱学习的斜杆青年。
---
