# 下载安装

## 1.进入官网地址下载安装包

点击下载：<https://nodejs.org/en>

## 2.安装程序

### （1）下载完成后，双击安装包，开始安装 Node.js

![img](/doc/assets/node/install1.png)

### （2） 直接点【Next】按钮，此处可根据个人需求修改安装路径，修改完毕后继续点击【Next】按钮

![img](/doc/assets/node/install2.png)

### （3）可根据自身需求进行，此处我选择默认安装，继续点击【Next】按钮

![img](/doc/assets/node/install3.png)

### （4）不选中，直接点击【Next】按钮

![img](/doc/assets/node/install4.png)

### （5）点击【Install】按钮进行安装

![img](/doc/assets/node/install5.png)

### （6）安装完毕，点击【Finish】按钮

![img](/doc/assets/node/install6.png)

### （7）测试安装是否成功，按下【win+R】键，输入 cmd，打开 cmd 窗口

```
node -v // 显示 node.js 版本
npm -v // 显示npm版本
```

![img](/doc/assets/node/install7.png)

::: warning
成功显示版本说明安装成功
:::
