import { defineConfig } from "vitepress";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "淳港的个人空间",
  description: "淳港的个人空间：好好学习，天天向上",
  base: "/doc/",
  head: [["link", { rel: "icon", href: "/doc/assets/logo.jpg" }]],
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: "/assets/logo.jpg",
    outline: {
      label: "目录",
      level: [2, 6],
    },
    nav: [
      { text: "首页", link: "/" },
      {
        text: "个人成长",
        items: [
          {
            text: "node",
            link: "/node/index",
          },
          {
            text: "vue",
            link: "/vue/index",
          },
          {
            text: "html",
            link: "/html/index",
          },
          {
            text: "js",
            link: "/js/index",
          },
          {
            text: "css",
            link: "/css/index",
          },
        ],
      },
    ],

    sidebar: {
      "/vue/": [
        {
          text: "鱿鱼西，永远滴神",
          items: [
            { text: "Naruto", link: "/vue/vue2/index" },
            { text: "One Piece", link: "/vue/vue3/index" },
          ],
        },
      ],
      "/node/": [
        {
          text: "使用教程",
          collapsed: true,
          items: [{ text: "安装", link: "/node/install" }],
        },
        {
          text: "包管理工具",
          collapsed: true,
          items: [
            { text: "npm", link: "/node/npm/index" },
            { text: "yarn", link: "/vue/vue3/index" },
            { text: "pnpm", link: "/vue/vue3/index" },
            { text: "yarn", link: "/vue/vue3/index" },
          ],
        },
      ],
    },
    socialLinks: [
      { icon: "github", link: "https://github.com/vuejs/vitepress" },
    ],
    search: {
      provider: "local",
    },
  },
});
